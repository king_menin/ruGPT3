# RuGPT3
Note, for running we should install https://github.com/ptillet/torch-blocksparse
Also Torch-Blocksparse depends on CUDA 10.1 and the Triton language and compiler, which requires llvm-9 (already installed on chisto).

## Code changes
* Add default arguments to `arguments.py`
* Add sparse attention (just rewrite torch-blocksparse implementation of Sparse MultiHead Attention (https://arxiv.org/abs/1904.10509) for mpu):
    * `GPT2ParallelSparseSelfAttention` at `mpu/transformer.py`
    * Modify `GPT2ParallelTransformer` and `GPT2ParallelTransformerLayer` at `mpu/transformer.py`
* Solve memory issue - modify line 144 at `data_utils/__init__.py` (add  weighted=False)
    * `ds = GPT2Dataset(ds, max_seq_len=seq_length, weighted=False)`
