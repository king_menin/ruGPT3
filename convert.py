from collections import OrderedDict
from utils import get_checkpoint_tracker_filename, get_checkpoint_name, ensure_directory_exists
import torch
import json
import os
import argparse


def convert_from_transformers2mpu(
        src_dir, dst_dir, make_vocab_size_divisible_by=128,
        model_parallel_world=1, verbose=True):
    """
    Convert huggingface transformers (2.8.0 version) weights checkpoint to mpu model dict for gpt2.

    Parameters
    ----------
    src_dir : str
        Path to dir with transformers checkpoint.
    dst_dir : str
        Path to dir for save mpu checkpoint.
    make_vocab_size_divisible_by : int, default=128
        Pad the vocab size to be divisible by this value.
        This is added for computational efficieny reasons (in mpu original realization).
        We can set this param to 1 (without padded vocab).
    model_parallel_world : int, default=1
        Size of the model parallel in mpu.
    verbose : bool, default=True
        Print some info or not.
    """
    with open(os.path.join(src_dir, "config.json"), "r") as file:
        tr_config = json.load(file)
    n_layers = tr_config["n_layer"]
    checkpoint_name = get_checkpoint_name(dst_dir, 0, True, 0)
    t_w = torch.load(os.path.join(src_dir, "pytorch_model.bin"))
    sd = {"model": map_weights(
        t_w, n_layers,
        make_vocab_size_divisible_by=make_vocab_size_divisible_by,
        model_parallel_world=model_parallel_world,
        verbose=verbose)[0]}
    ensure_directory_exists(checkpoint_name)
    torch.save(sd, checkpoint_name)
    # Write release to iteration tracker file for no load rng and optim
    tracker_filename = get_checkpoint_tracker_filename(dst_dir)
    with open(tracker_filename, "w") as file:
        file.write("release")
    if verbose:
        print('Successfully saved {}'.format(checkpoint_name))


def map_weights(t_w, n_layers, mpu_w=None, make_vocab_size_divisible_by=128, model_parallel_world=1, verbose=False):
    """
    Map huggingface transformers (2.8.0 version) weights checkpoint to mpu model dict.

    Parameters
    ----------
    t_w : OrderedDict[str, torch.Tensor]
        State dict of model from transformers.
    n_layers : int
        Number of layers.
    mpu_w : OrderedDict[str, torch.Tensor], default=None.
        State dict of model for mpu.
    make_vocab_size_divisible_by : int, default=128
        Pad the vocab size to be divisible by this value.
        This is added for computational efficieny reasons (in mpu original realization).
        We can set this param to 1 (without padded vocab).
    model_parallel_world : int, default=1
        Size of the model parallel in mpu.
    verbose : bool, default=False
        Print skipped layers or not.

    Returns
    -------
    (mpu_w, total_ignored)
        mpu_w : OrderedDict[str, torch.Tensor]
            State dict of model for mpu.
        all_ignored : list[str]
            List of ignored layers.
    """
    # TODO: model_parallel_world > 1
    if model_parallel_world != 1:
        raise NotImplemented("Conversion is implemented only for model_parallel_world 1.")
    if mpu_w is None:
        mpu_w = OrderedDict()
    all_ignored = []
    # embeddings
    mpu_w[f"word_embeddings.weight"] = t_w[f"transformer.wte.weight"]
    mpu_w[f"position_embeddings.weight"] = t_w[f"transformer.wpe.weight"]
    # Pad vocab
    num_tokens, model_dim = t_w[f"transformer.wte.weight"].shape
    before = num_tokens
    after = before
    multiple = make_vocab_size_divisible_by * model_parallel_world
    while (after % multiple) != 0:
        after += 1
    pad = torch.rand(after - before, model_dim, device=mpu_w[f"word_embeddings.weight"].device)
    mpu_w[f"word_embeddings.weight"] = torch.cat([mpu_w[f"word_embeddings.weight"], pad])
    for n_layer in range(n_layers):
        mpu_w, ignored_layers = map_layer_weights(t_w, n_layer, mpu_w)
        all_ignored += ignored_layers
    mpu_w["transformer.final_layernorm.weight"] = t_w["transformer.ln_f.weight"]
    mpu_w["transformer.final_layernorm.bias"] = t_w["transformer.ln_f.bias"]
    all_ignored += ["lm_head.weight"]
    if verbose:
        for layer in all_ignored:
            print(f"Skip layer '{layer}'")
    return mpu_w, all_ignored


def map_layer_weights(t_w, idx, mpu_w=None):
    if mpu_w is None:
        mpu_w = OrderedDict()
    mpu_w[f"transformer.layers.{idx}.input_layernorm.weight"] = t_w[
        f"transformer.h.{idx}.ln_1.weight"]
    mpu_w[f"transformer.layers.{idx}.input_layernorm.bias"] = t_w[
        f"transformer.h.{idx}.ln_1.bias"]
    #  ('transformer.h.1.attn.bias', torch.Size([1, 1, 2048, 2048])),
    # ('transformer.h.1.attn.masked_bias', torch.Size([])),
    mpu_w[f"transformer.layers.{idx}.attention.query_key_value.weight"] = t_w[
        f"transformer.h.{idx}.attn.c_attn.weight"].T
    mpu_w[f"transformer.layers.{idx}.attention.query_key_value.bias"] = t_w[
        f"transformer.h.{idx}.attn.c_attn.bias"]

    mpu_w[f"transformer.layers.{idx}.attention.dense.weight"] = t_w[
        f"transformer.h.{idx}.attn.c_proj.weight"]
    mpu_w[f"transformer.layers.{idx}.attention.dense.bias"] = t_w[
        f"transformer.h.{idx}.attn.c_proj.bias"]

    mpu_w[f"transformer.layers.{idx}.post_attention_layernorm.weight"] = t_w[
        f"transformer.h.{idx}.ln_2.weight"].T
    mpu_w[f"transformer.layers.{idx}.post_attention_layernorm.bias"] = t_w[
        f"transformer.h.{idx}.ln_2.bias"]

    mpu_w[f"transformer.layers.{idx}.mlp.dense_h_to_4h.weight"] = t_w[
        f"transformer.h.{idx}.mlp.c_fc.weight"].T
    mpu_w[f"transformer.layers.{idx}.mlp.dense_h_to_4h.bias"] = t_w[
        f"transformer.h.{idx}.mlp.c_fc.bias"]

    mpu_w[f"transformer.layers.{idx}.mlp.dense_4h_to_h.weight"] = t_w[
        f"transformer.h.{idx}.mlp.c_proj.weight"].T
    mpu_w[f"transformer.layers.{idx}.mlp.dense_4h_to_h.bias"] = t_w[
        f"transformer.h.{idx}.mlp.c_proj.bias"]
    return mpu_w, [f'transformer.h.{idx}.attn.bias', f'transformer.h.{idx}.attn.masked_bias']


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--src_dir", type=str, required=True, help="Path to dir with transformers checkpoint.")
    parser.add_argument(
        "--dst_dir", type=str, required=True, help="Path to dir for save mpu checkpoint.")
    parser.add_argument(
        "--make_vocab_size_divisible_by", type=int, default=128,
        help="Path to dir for save mpu checkpoint.")
    parser.add_argument(
        "--model_parallel_world", type=int, default=1,
        help="Pad the vocab size to be divisible by this value."
        "This is added for computational efficieny reasons (in mpu original realization)."
        "We can set this param to 1 (without padded vocab).")
    parser.add_argument(
        "--verbose", action='store_true', help="Print some info or not.")
    args = parser.parse_args()
    convert_from_transformers2mpu(**vars(args))


if __name__ == "__main__":
    main()
