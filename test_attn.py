import os

import torch

import mpu
from mpu.transformer import GPT2ParallelSparseSelfAttention


def initialize_distributed(args=None):
    """Initialize torch.distributed."""
    if args is None:
        from arguments import get_args
        args = get_args()
    # Manually set the device ids.
    device = args.rank % torch.cuda.device_count()
    if args.local_rank is not None:
        device = args.local_rank
    torch.cuda.set_device(device)
    # Call the init process
    init_method = 'tcp://'
    master_ip = os.getenv('MASTER_ADDR', 'localhost')
    master_port = os.getenv('MASTER_PORT', '6000')
    init_method += master_ip + ':' + master_port
    torch.distributed.init_process_group(
        backend=args.distributed_backend,
        world_size=args.world_size, rank=args.rank,
        init_method=init_method)

    # Set the model-parallel / data-parallel communicators.
    mpu.initialize_model_parallel(args.model_parallel_size)


if __name__ == "__main__":
    from mpu.transformer import unscaled_init_method

    initialize_distributed()
    bs = 2
    seq_len = 128
    hidden_size = 128
    num_attention_heads = 2
    attention_dropout_prob = 0.1
    output_dropout_prob = 0.1
    init_method_std = 0.02
    init_method = unscaled_init_method(init_method_std)
    output_layer_init_method = None

    dense = GPT2ParallelSparseSelfAttention(
        hidden_size, num_attention_heads,
        attention_dropout_prob, output_dropout_prob,
        init_method, output_layer_init_method).cuda()
    sample = torch.rand(bs, seq_len, hidden_size).cuda()

    ltor_mask = torch.randint(0, 2, (1, 1, seq_len, seq_len)).cuda()

    dense_out = dense(sample, ltor_mask.float())
    print(dense_out, dense_out.shape)
